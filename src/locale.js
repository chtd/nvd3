nv.getLocale = function(locale) { return locale; };

nv.locale = nv.getLocale({
  controls: {
    stacked: 'Stacked',
    stack_percent: 'Stack %',
    grouped: 'Grouped',
    stream: 'Stream',
    expanded: 'Expanded',
    rescale: 'Re-scale y-axis'
  },
  stackedAreaControls: {
    stacked: 'Stacked',
    stack_percent: 'Stack %',
    stream: 'Stream',
    expanded: 'Expanded'
  },
  legend: {
      at: ' at ',
      on: ' on ',
      axisLeft: ' (left axis)',
      axisRight: ' (right axis)'
  },
  nodata: 'No Data Available.'
});
